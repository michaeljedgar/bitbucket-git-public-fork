"""Logs post hooks from bitbucket."""

import logging
import webapp2

class BitbucketLogger(webapp2.RequestHandler):
  def post(self):
    logging.info("Request data: " + repr(self.request.POST))
    self.response.status_int = 200

app = webapp2.WSGIApplication([
    ('/bitbucket', BitbucketLogger),
], debug=True)

def main():
    run_wsgi_app(app)

if __name__ == '__main__':
    main()
